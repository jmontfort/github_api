# TASK #

Exercise:

Using the GitHub API write a program that does the following:

- Takes user input for a search term and lists the top 5 GitHub repositories matching that search term.

- For the returned repositories list the 5 most recent commits for each repository.

The output for repositories at a minimum should show:

    -- Owner name.

    -- Repository name.

    -- Repository URL.

    -- Created date.

    -- Last push date.

The output for commits should show:

    -- Committer name.

    -- Message (if applicable).

    -- SHA hash.

    -- Date.

Restrictions:

The only other restriction is that the program is written in C#. You are free to make any other technology choices (Console App, Website, .NET Core etc.).

You can spend as much time on it as you wish, however 1-2 hours should be more than enough time.

References:

 GitHub API Search endpoint: https://developer.github.com/v3/search/#search-repositories

 GitHub API Commits endpoint: https://developer.github.com/v3/repos/commits/

Tips:

 The GitHub API requires a User-Agent header: https://developer.github.com/v3/#user-

agent-required

 You may need to enable UnsafeHeaderParsing:

https://social.msdn.microsoft.com/Forums/vstudio/en-US/2b02ad0e- 623b-457d- ab9d-

fa8b509b349e/the-famous- httpwebrequest-the- server-committed- a-protocol- violation-

sectionresponsestatusline?forum=csharpgeneral
