﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Github_API.API.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Github_API.API.Controllers
{
    [Route("api/[controller]")]
    public class GithubApiController : Controller
    {
        // GET: api/values
        [HttpGet("/api/GithubAPI/Get/searchRepo={searchRepo}")]
        public async Task<JsonResult> Get(string searchRepo)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
            client.DefaultRequestHeaders.Add("User-Agent", ".Awesome-Octocat-App");

            var stringTask = await client.GetStringAsync(
                String.Format("https://api.github.com/search/repositories?q={0}&sort=stars&order=desc&page=1", searchRepo));

            RootObject rootObject = JsonConvert.DeserializeObject<RootObject>(stringTask);
            var myList = rootObject.items.Take(5);
            return Json(myList);
        }

        // GET: api/values
        [HttpGet("/api/GithubAPI/Get/userName={userName}&repoName={repoName}")]
        public async Task<JsonResult> Get(string userName, string repoName)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
            client.DefaultRequestHeaders.Add("User-Agent", ".Awesome-Octocat-App");

            var stringTask = await client.GetStringAsync(
                String.Format("https://api.github.com/repos/{0}/{1}/commits", userName, repoName));

            List<RootObject> rootObjectList = JsonConvert.DeserializeObject<List<RootObject>>(stringTask);
            var orderedByDateList = rootObjectList.OrderByDescending(x => x.commit.committer.date);

            return Json(rootObjectList.Take(5));
        }

    }

    public class MyStok 
    {
    }
}
