﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Github_API.API.Models
{
    public class Committer
    {
        public string name { get; set; }
        public string email { get; set; }
        public string date { get; set; }
    }
}
