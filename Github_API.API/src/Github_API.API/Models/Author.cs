﻿namespace Github_API.API.Models
{
    public class Author
    {
        public string name { get; set; }
        public string email { get; set; }
        public string date { get; set; }
    }
}