﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Github_API.API;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Github_API.Tests
{
    public class Utils
    {
        public static IWebHostBuilder GetHostBuilder(string[] args)
        {
            var config = new ConfigurationBuilder()
                       .AddCommandLine(args)
                       .AddEnvironmentVariables(prefix: "ASPNETCORE_")
                       .Build();

            return new WebHostBuilder()
                .UseConfiguration(config)
                .UseKestrel()
                .UseStartup<Startup>();
        }
    }
}
