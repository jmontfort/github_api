﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Github_API.API;
using Github_API.API.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Github_API.Tests.IntegrationTests
{
    [TestFixture]
    public class GithubApiControllerTests
    {
        private HttpClient _client;
        private string _request;

        [SetUp]
        public void Setup()
        {
            var basePath = PlatformServices.Default.Application.ApplicationBasePath;
            var projectPath = Path.GetFullPath(Path.Combine(basePath, "../../../../Github_API.Tests"));

            var server = new TestServer(Utils.GetHostBuilder(new string[] { })
                .UseContentRoot(projectPath)
                .UseEnvironment("Development")
                .UseStartup<Startup>());

            _client = server.CreateClient();
            _request = "api/GithubApi/";
        }

        [Test]
        public async Task Get_EnsureSuccessStatusCode_SearchRepos_GithubAPIController()
        {
            string searchStr = "tetris";
            var response = await _client.GetAsync(_request + String.Format("Get/searchRepo={0}", searchStr));
            response.EnsureSuccessStatusCode();

            Assert.IsTrue(true);
        }

        [Test]
        public async Task Get_ReturnsAListOfTop5Repos_GithubAPIController()
        {
            var searchStr = "tetris";
            var response = await _client.GetAsync(_request + String.Format("Get/searchRepo={0}", searchStr));

            var fetched = await response.Content.ReadAsStringAsync();
            var fetchedItems = JsonConvert.DeserializeObject<List<Item>>(fetched);

            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsTrue(fetchedItems.Count == 5);
        }

        [Test]
        public async Task Get_EnsureSuccessStatusCode_RepoByUserName_GithubAPIController()
        {
            var userName = "NancyFx";
            var repoName = "Nancy";
            var response = await _client.GetAsync(_request + String.Format("Get/userName={0}&repoName={1}", userName, repoName));
            response.EnsureSuccessStatusCode();

            Assert.IsTrue(true);
        }

        [Test]
        public async Task Get_ReturnsAListOfTop5RecentCommitsPerRepoPerUser_GithubAPIController()
        {
            var userName = "NancyFx";
            var repoName = "Nancy";
            var response = await _client.GetAsync(_request + String.Format("Get/userName={0}&repoName={1}", userName, repoName));

            var fetched = await response.Content.ReadAsStringAsync();
            var fetchedItems = JsonConvert.DeserializeObject<List<RootObject>>(fetched);

            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsTrue(fetchedItems.Count == 5);
        }

        [Test]
        public async Task Get_EnsureWeHaveTheMostRecentCommitAtTheTop_GithubAPIController()
        {
            var userName = "NancyFx";
            var repoName = "Nancy";
            var response = await _client.GetAsync(_request + String.Format("Get/userName={0}&repoName={1}", userName, repoName));

            var fetched = await response.Content.ReadAsStringAsync();
            var fetchedItems = JsonConvert.DeserializeObject<List<RootObject>>(fetched);

            var firstItem = fetchedItems[0];
            var secondItem = fetchedItems[1];

            Assert.IsTrue(response.IsSuccessStatusCode);
            Assert.IsTrue(Convert.ToDateTime(firstItem.commit.committer.date) > Convert.ToDateTime(secondItem.commit.committer.date));
        }
    }
}
